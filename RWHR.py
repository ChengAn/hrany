import sys
import os
import win32com.client
from Folder import * 
from Item import *
from HR import *
from win32com.mapi import mapitags
import binascii
from FilterRule import *

def load_Filter(filename,filter_set):
    file=open(filename,'r')
    for line in file:
        filter_set.append(line.strip('\n'))
    file.close()

def save_Filter(filename,filter_set):
    file=open(filename,'w')
    for line in filter_set:
        file.write(line+"\n")
    file.close()    

def hr_main(target_str,pinfo_set,add_pinfo2list):

     target_folder=[]
     
     if win32com.client.gencache.is_readonly == True:
          win32com.client.gencache.is_readonly = False
          win32com.client.gencache.Rebuild()
     win32com.client.gencache.EnsureDispatch("Outlook.Application")
     outlook =win32com.client.Dispatch("Outlook.Application")
     mapi=outlook.GetNamespace("MAPI")

# pst
# --->mail folder 
     pfolders = mapi.Folders
     for pfolder in pfolders:
          #print(pfolder.Name)
          store = pfolder.Store
          if(not store.IsDataFileStore):
               continue
                    #PR_IPM_OUTBOX_ENTRYID PR_IPM_WASTEBASKET_ENTRYID PR_IPM_SENTMAIL_ENTRYID
          filter_prop_tag=[0x35E20102,0x35E30102,0x35E40102]
          filter_eid=[]
          for tag in filter_prop_tag:
               prop= "http://schemas.microsoft.com/mapi/proptag/0x%8X" % (tag)
               try:
                    eid = store.PropertyAccessor.GetProperty(prop)
               except:
                    eid=[]
               seid=""
               for i in range(0,len(eid)):
                    seid = "%s%02X" %(seid,eid[i])
               filter_eid.append(seid)
          
          mfolders = pfolder.Folders
          for mfolder in mfolders:
               #print(mfolder.Name+":"+mfolder.EntryID)
               if (mfolder.EntryID not in filter_eid) and mfolder:
                    search_folder(mfolder,target_str,target_folder)

     for folder in target_folder:
          print("Search folder :"+folder.Name)
     
     if(len(target_folder)==0):
          print("Not found folder")
          return "請確認郵件目錄"
     target_items = target_folder[0].Items

     f_rule = FilterRule()

     f_per=[]
     load_Filter("Filter.txt",f_per)
     
     if len(f_per) < 5:
         return "請確認篩選設定 個人設定並存檔離開"
     f_rule.f_years = f_per[0]
     f_rule.f_m=f_per[1]
     f_rule.f_f=f_per[2]
     f_rule.f_normal=f_per[3]
     f_rule.f_RDSS=f_per[4]        
     load_Filter("MS.txt",f_rule.f_ms)
     load_Filter("BS.txt",f_rule.f_bs)
     load_Filter("DEP.txt",f_rule.f_dep)
     load_Filter("JOB.txt",f_rule.f_job)
     #print(f_rule)

     fid=0
     print("Filter.....start")
     for item in target_items:
          #print(item.Subject)
          body=item.Body

          #debug+
          #filename="{0}.txt".format(fid)
          #fid=fid+1
          #file=open(filename,'wb')
          #encode_body=body.encode("utf8")
          #file.write(encode_body)
          #file.close()
          #debug-
          
          name=search_Name(body)
          if(name == None):
                    continue
          #print(name)
          years=search_Years(body)
          ms=[]
          search_MS(body,ms)
          bs=[]
          search_BS(body,bs)
          mf=search_MF(body)         
          djobs=[]
          search_desired_job(body,djobs)
          n_link=[]
          h_link=[]
          search_attached_files(body,n_link,h_link)
            
          pinfo = PersonInfo(item,name,years,ms[0],ms[1],bs[0],bs[1],mf,djobs,n_link,h_link)
          pinfo.search_RDSS()
          #print(pinfo.RDSS)
          if(f_rule.filter_pinfo(pinfo)):
              pinfo_set.append(pinfo)
              add_pinfo2list(pinfo)
     print("Filter.....end")
     return None
        
def hr_send_mail(item_set):
    if win32com.client.gencache.is_readonly == True:
        win32com.client.gencache.is_readonly = False
        win32com.client.gencache.Rebuild()
    win32com.client.gencache.EnsureDispatch("Outlook.Application")
    outlook =win32com.client.Dispatch("Outlook.Application")
    sent=outlook.CreateItem(win32com.client.constants.olMailItem)
    for i in item_set:
        sent.Attachments.Add(Source=i)
    return sent
