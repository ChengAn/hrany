class PersonInfo:
	def __init__(self,Item,Name,Years,MS,MSdep,BS,BSdep,MF,DJobs,NLink,HLink):
		self.Name=Name
		self.Years=Years
		self.MS=MS
		self.MSdep=MSdep
		self.BS=BS
		self.BSdep=BSdep
		self.MF=MF
		self.DJobs=DJobs
		self.vardata=None
		self.Item=Item
		self.NLink=NLink
		self.HLink=HLink
		self.AutobioPoint=[]
		self.RDSS=0

	def __str__(self):
		return "Name={0} Years={1} MS={2} dep={5} BS={3} dep={6} MF={4}".format(self.Name,self.Years,self.MS,self.BS,self.MF,self.MSdep,self.BSdep)

	def search_RDSS(self):
		body=self.Item.Body
		per=body.find("個 人 資 料")
		idd=body.find("特殊身分",per,per+256)
		if idd ==-1:
			return
		sstr=body[idd:idd+96].split('\u0009',2)
		if sstr[1].find("研發替代役") != -1:
			self.RDSS=1
	def search_autobio(self):
		body=self.Item.Body
		auto=body.find("自 傳")
		if auto == -1:
			return
		start_p=body.find('\u00A0\u0009\u0020\u000D\u000A',auto)
		if start_p == -1:
			return
		end_p=body.find('\u0009\u000D\u000A\u00A0',start_p)
		if end_p ==-1 or start_p>end_p:
			return
		self.AutobioPoint.append(start_p)
		self.AutobioPoint.append(end_p)
	
def search_Name(body):
        if(body.find("vip.104.com.tw")== -1):
                return None
        id=body.find("代碼")
        raw_str=body[id-48:id]
        sstr=raw_str.rsplit('\u3000')
        raw_name=sstr[0].rsplit('"',1)
        return raw_name[1].replace('\u003F','\u0058')

def search_Years(body):
        id=body.find("代碼")
        raw_str=body[id-48:id]
        sstr=raw_str.rsplit('\u3000')
        raw_years=sstr[1].rsplit('歲',1)
        return raw_years[0]

def search_MF(body):
        id=body.find("代碼")
        raw_str=body[id-48:id]
        sstr=raw_str.rsplit('\u3000')
        mf=sstr[2].split('\u0020') 
        return mf[0]        

def search_MS(body,edu_set):
        edu=body.find("教 育 背 景")
        hedu=body.find("最高學歷",edu,edu+256)
        sstr=body[hedu:hedu+96].split('\u0009',2)
        sch=""
        dep=""        
        if(sstr[1].find("碩士") != -1):
                hedus=body.find("最高：",edu,edu+256)
                if(hedus != -1):
                        sstr=body[hedus:hedus+96].split('\u0009',2)
                        st=sstr[1].replace('\u0020','').split('\u3000',1)
                        sch=st[0]
                        st=st[1].split('\uFF08',1)
                        dep=st[0]
        edu_set.append(sch)
        edu_set.append(dep)     

def search_BS(body,edu_set):
        edu=body.find("教 育 背 景")
        hedu=body.find("最高學歷",edu,edu+256)
        sstr=body[hedu:hedu+96].split('\u0009',2)
        sch=""
        dep=""
        if(sstr[1].find("碩士") != -1):
                hedus=body.find("次高：",edu,edu+256)
                if(hedus != -1):
                        sstr=body[hedus:hedus+96].split('\u0009',2)
                        st=sstr[1].replace('\u0020','').split('\u3000',1)
                        sch=st[0]
                        st=st[1].split('\uFF08',1)
                        dep=st[0]
        elif(sstr[1].find("大學") != -1):
                hedus=body.find("最高：",edu,edu+256)
                if(hedus != -1):
                        sstr=body[hedus:hedus+96].split('\u0009',2)
                        st=sstr[1].replace('\u0020','').split('\u3000',1)
                        sch=st[0]
                        st=st[1].split('\uFF08',1)
                        dep=st[0]
        edu_set.append(sch)
        edu_set.append(dep)
        
def search_desired_job(body,job_set):
        job=body.find("求 職 條 件")
        
        hjob=body.find("希望職稱",job,job+1024)
        if(hjob != -1):
                sstr=body[hjob:hjob+96].split('\u0009',2)
                jstr=[]
                if(sstr[1].find('\u3001') != -1):
                        jstr=sstr[1].split('\u3001')
                else:
                        jstr.append(sstr[1])
                for j in jstr:
                        job_set.append(j.replace('\u0020',''))
        
        hjob=body.find("希望職類",job,job+1024)
        if(hjob != -1):
                sstr=body[hjob:hjob+96].split('\u0009',2)
                jstr=[]
                if(sstr[1].find('\u3001') != -1):
                        jstr=sstr[1].split('\u3001')
                else:
                        jstr.append(sstr[1])
                for j in jstr:
                        job_set.append(j.replace('\u0020','').replace('\u00A0',''))

def search_attached_files(body,n_link,h_link):
       att=body.find("附 件")
       if att == -1:
               return
       hlink=body.find("HYPERLINK",att,att+1024)
       end_hlink=body.find("104人力銀行 版權所有")
       end_hlink2=body.find("符合職務：",att)
       if end_hlink2 != -1 and  end_hlink2 < end_hlink:
               end_hlink = end_hlink2
       hstr=body[hlink:end_hlink].split("HYPERLINK")
       for n in range(1,len(hstr)):
               lstr=hstr[n].split('\u0022')
               nstr=lstr[2].split('\u0009')
               n_link.append(nstr[0])
               h_link.append(lstr[1])
                
def filter_set(istr,f_set):
        if(f_set[0].find("SKIP") != -1):
                return True
        match=0
        if(istr==""):
                if(f_set[1].find("EMPTY") != -1):
                        return True
                else:
                        return False
        for m in f_set[2:]:
                if(istr.find(m) != -1):
                        match=1
                        break
        if(match):
                return True
        else:
                return False        

def filter_pinfo(info,f_years,f_m,f_f,f_ms,f_bs,f_dep,f_job):
        if int(info.Years,10) > int(f_years,10):
                return False
        if(info.MF=="男性" and f_m=="M_NO"):
                return False
        if(info.MF=="女性" and f_f=="F_NO"):
                return False
        if(filter_set(info.MS,f_ms)==False):
               return False
        if(filter_set(info.BS,f_bs)==False):
               return False
        if(filter_set(info.MSdep,f_dep)==False):
               return False
        if(filter_set(info.BSdep,f_dep)==False):
               return False
        mj = False
        for j in info.DJobs:
                if(filter_set(j,f_job)):
                        mj=True
                        break
        if(mj==False):                        
               return False           
        
        return True
        
        

                
