from tkinter import *

class ScrolledFrame(Frame):
    def __init__(self, root):

        Frame.__init__(self, root)
        self.canvas = Canvas(self, borderwidth=0, background="white")
        self.frame = Frame(self.canvas,background="white")
        self.vsb = Scrollbar(self, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.vsb.set)

        self.vsb.pack(side=RIGHT, fill=BOTH)
        self.canvas.pack(side=LEFT, fill=BOTH, expand=1)
        self.canvas.create_window((4,4), window=self.frame, anchor="nw", 
                                  tags="self.frame")

        self.frame.bind("<Configure>", self.OnFrameConfigure)
        self.data=[]

        #self.canvas.bind_all("<MouseWheel>", self._on_mousewheel)

    #def _on_mousewheel(self, event):
        #print(event.delta)
        #self.canvas.yview_scroll(-1*20, "units")

    def insert(self,obj):
        self.data.append(obj)
        obj.pack(side=TOP,fill=X)
        self.frame.update()

    def clear(self):
        for obj in self.data:
            self.data.remove(obj)
            obj.destroy()
        self.frame.update()
            
    def OnFrameConfigure(self, event):
        '''Reset the scroll region to encompass the inner frame'''
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))
