class FilterRule:
    def __init__(self):
        self.f_years="100"
        self.f_m="M_YES"
        self.f_f="F_YES"
        self.f_normal="NORMAL_YES"
        self.f_RDSS="RDSS_YES"
        self.f_ms=[]
        self.f_bs=[]
        self.f_dep=[]
        self.f_job=[]
    def __str__(self):
        istr=""
        istr=istr+"years :"+self.f_years+"\n"
        istr=istr+"mem :"+self.f_m+"\n"
        istr=istr+"f :"+self.f_f+"\n"
        istr=istr+"ms :\n"
        for f in self.f_ms:
            istr=istr+f+" \n"
        istr=istr+"bs :\n"
        for f in self.f_bs:
            istr=istr+f+" \n"
        istr=istr+"dep :\n"
        for f in self.f_dep:
            istr=istr+f+" \n"
        istr=istr+"job :\n"
        for f in self.f_job:
            istr=istr+f+" \n"
        return istr

    def filter_set(self,istr,f_set):
        if(f_set[0].find("SKIP") != -1):
            return True
        match=0
        if(istr==""):
            if(f_set[1].find("EMPTY") != -1):
                    return True
            else:
                    return False
        for m in f_set[2:]:
            if(istr.find(m) != -1):
                    match=1
                    break
        if(match):
            return True
        else:
            return False
        
    def filter_pinfo(self,info):
        if int(info.Years,10) > int(self.f_years,10):
            #print(info.Years)
            return False
        if(info.MF=="男性" and self.f_m=="M_NO"):
            #print(info.MF)
            return False
        if(info.MF=="女性" and self.f_f=="F_NO"):
            #print(info.MF)
            return False
        if(info.RDSS==0 and self.f_normal=="NORMAL_NO"):
            return False
        if(info.RDSS and self.f_RDSS=="RDSS_NO"):
            return False
        if(self.filter_set(info.MS,self.f_ms)==False):
            #print(info.MS)
            return False
        if(self.filter_set(info.BS,self.f_bs)==False):
            #print(info.BS)
            return False
        if(self.filter_set(info.MSdep,self.f_dep)==False):
            #print(info.MSdep)
            return False
        if(self.filter_set(info.BSdep,self.f_dep)==False):
            #print(info.BSdep)
            return False
        mj = False
        for j in info.DJobs:
            if(self.filter_set(j,self.f_job)):
                mj=True
                break
        if(mj==False):                        
            return False           
        return True        
        
        
        
        
