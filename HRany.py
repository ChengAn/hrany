from tkinter import *
from tkinter.filedialog import *
from RWHR import *
from HR import *
from ScrollFrame import *
import urllib.request
import shutil
from mimetypes import guess_extension
import urllib
import atexit
import base64
import re
import tkinter.messagebox
pinfo_set=[]

def exit_handler():
	print("Exit HRany")
	if os.path.exists("tmp"):
		shutil.rmtree("tmp")

def open_mail_callback(pinfo):
        pinfo.Item.Display()

def open_attached_file_callback(pinfo,n):
	#print(pinfo.HLink[n]+":"+pinfo.NLink[n])

	auth_handler = urllib.request.HTTPBasicAuthHandler()
	auth_handler.add_password(realm=None, uri=pinfo.HLink[n], user="1212312ad35", passwd="456vxcvxcv7")
	opener = urllib.request.build_opener(auth_handler)
	opener.add_handler(auth_handler)
	result = opener.open(pinfo.HLink[n])

	filename= re.findall("filename=(\S+)", result.info()['Content-Disposition'])
	if len(filename) > 0 and (filename[0].find('.') != -1):
                estr = filename[0].rsplit('.')
                extension=estr[len(estr)-1]
	else:
                extension="txt"
	extension = "."+extension
        #extension = guess_extension(result.info()['Content-Type'])
	#print(extension)
                
	if not os.path.exists("tmp"):
		os.makedirs("tmp")
	
	dirlocal=os.getcwd()+"/tmp/"+pinfo.NLink[n]+extension
	#print(dirlocal)
	with open(dirlocal, 'b+w') as f:
		shutil.copyfileobj(result,f)
	os.startfile(dirlocal)

def open_autobio_callback(pinfo):
	if len(pinfo.AutobioPoint) ==0:
		pinfo.search_autobio()

	if len(pinfo.AutobioPoint) == 2:
		#print(pinfo.AutobioPoint)
		sstr=pinfo.Item.Body[pinfo.AutobioPoint[0]:pinfo.AutobioPoint[1]]
		atop=Toplevel()
		atop.title("自傳")
		autobio=Text(atop)
		autobio.insert(INSERT,sstr)
		autobio.pack()

def add_pinfo2list(pinfo):
	pinfo_frame=LabelFrame(list_lframe.frame)

	pinfo.vardata = IntVar()
	pinfo_check_lab=Checkbutton(pinfo_frame,variable = pinfo.vardata)
	pinfo_check_lab.grid(row=0,column=0)
	pinfo_open_lab=Button(pinfo_frame,text="開啟信件",command=lambda:open_mail_callback(pinfo))
	pinfo_open_lab.grid(row=0,column=1)
	pinfo_name_lab=Label(pinfo_frame,text=pinfo.Name,width=10)
	pinfo_name_lab.grid(row=0,column=2)
	pinfo_years_lab=Label(pinfo_frame,text=pinfo.Years,width=5)
	pinfo_years_lab.grid(row=0,column=3)
	if pinfo.RDSS :
		pinfo_mf_lab=Label(pinfo_frame,text=pinfo.MF+"研替",width=5)
	else:
		pinfo_mf_lab=Label(pinfo_frame,text=pinfo.MF,width=5)
	pinfo_mf_lab.grid(row=0,column=4)	
	pinfo_ms_lab=Label(pinfo_frame,text=pinfo.MS,width=20)
	pinfo_ms_lab.grid(row=0,column=5)
	pinfo_msdep_lab=Label(pinfo_frame,text=pinfo.MSdep,width=20)
	pinfo_msdep_lab.grid(row=0,column=6)
	pinfo_bs_lab=Label(pinfo_frame,text=pinfo.BS,width=20)
	pinfo_bs_lab.grid(row=0,column=7)
	pinfo_bsdep_lab=Label(pinfo_frame,text=pinfo.BSdep,width=20)
	pinfo_bsdep_lab.grid(row=0,column=8)
	pinfo_djobs_lab=Label(pinfo_frame,text=pinfo.DJobs[0],width=10)
	pinfo_djobs_lab.grid(row=0,column=9)
	pinfo_autobio_but=Button(pinfo_frame,text="自傳",command=lambda:open_autobio_callback(pinfo),width=10)
	pinfo_autobio_but.grid(row=0,column=10)	
	for nl in range(0,len(pinfo.NLink)):
		pinfo_attached_but=Button(pinfo_frame,text=pinfo.NLink[nl][:5],command=lambda num=nl:open_attached_file_callback(pinfo,num),width=10)
		pinfo_attached_but.grid(row=0,column=11+nl)
	
	list_lframe.insert(pinfo_frame)

def start_filter_callback(ctl_set):
	for c in ctl_set:
		c.configure(state=DISABLED)
	
	list_lframe.clear()
	target_str=[]
	estr=search_entry.get()
	if estr != "":
		target_str.append(estr)
	err=hr_main(target_str,pinfo_set,add_pinfo2list)
	if err != None:
		tkinter.messagebox.showinfo("錯誤",err)

	for c in ctl_set:
		c.configure(state=NORMAL)

def save_select_callback():
	dirname = askdirectory()
	print(dirname)
	for pinfo in pinfo_set:
		if(pinfo.vardata.get()):
			pinfo.Item.SaveAs(dirname+"/"+pinfo.Name+".msg",win32com.client.constants.olMSG)

def mail_select_callback():
	item_set=[]
	for pinfo in pinfo_set:
		if(pinfo.vardata.get()):
			item_set.append(pinfo.Item)
	sent=hr_send_mail(item_set)
	sent.Display()

def all_check_callback(check_but):
	if(all_check.get()):
		for p in pinfo_set:
			p.vardata.set(0)
		check_but.configure(text="選擇全部")
		check_but.update()
		all_check.set(0)
	else:
		for p in pinfo_set:
			p.vardata.set(1)
		check_but.configure(text="取消全部")
		check_but.update()
		all_check.set(1)
		
class filter_personal:
        def __init__(self,root,title_str,filter_str):
                flabf=LabelFrame(root,text=title_str)
                flabf.pack(side=LEFT,fill=Y)
                self.filter_str = filter_str

                years_lab=Label(flabf,text="年齡上限")
                years_lab.grid(row=0,column=0)
                
                self.years = StringVar()                
                years_entry=Entry(flabf,textvariable=self.years,width=5,bd=5)
                years_entry.grid(row=0,column=1)
                mf_lab=Label(flabf,text="性別")
                mf_lab.grid(row=1,column=0)
                self.m=IntVar()
                m_checkbut=Checkbutton(flabf,variable=self.m,text="男性")
                m_checkbut.grid(row=1,column=1)
                self.f=IntVar()
                f_checkbut=Checkbutton(flabf,variable=self.f,text="女性")
                f_checkbut.grid(row=2,column=1)
                fset=[]


                mf_lab=Label(flabf,text="身分")
                mf_lab.grid(row=3,column=0)
                self.normal=IntVar()
                normal_checkbut=Checkbutton(flabf,variable=self.normal,text="一般")
                normal_checkbut.grid(row=3,column=1)
                self.RDSS=IntVar()
                RDSS_checkbut=Checkbutton(flabf,variable=self.RDSS,text="研發替代役")
                RDSS_checkbut.grid(row=4,column=1)


                load_Filter(filter_str,fset)
                if len(fset)>0:
                        self.years.set(fset[0])
                if(len(fset)>1 and fset[1]=="M_NO"):
                        self.m.set(0)
                else:
                        self.m.set(1)
                if(len(fset)>2 and fset[2]=="F_NO"):
                        self.f.set(0)
                else:
                        self.f.set(1)
                
                if(len(fset)>3 and fset[3]=="NORMAL_NO"):
                        self.normal.set(0)
                else:
                        self.normal.set(1)
                if(len(fset)>4 and fset[4]=="RDSS_NO"):
                        self.RDSS.set(0)
                else:
                        self.RDSS.set(1)
        def save_filter(self):
                f=[]
                f.append(self.years.get())
                if(self.m.get()):
                        f.append("M_YES")
                else:
                        f.append("M_NO")
                if(self.f.get()):
                        f.append("F_YES")
                else:
                        f.append("F_NO")
                if(self.normal.get()):
                        f.append("NORMAL_YES")
                else:
                        f.append("NORMAL_NO")
                if(self.RDSS.get()):
                        f.append("RDSS_YES")
                else:
                        f.append("RDSS_NO")                        
                save_Filter(self.filter_str,f)
		
class filter_class:
	def __init__(self,root,title_str,filter_str):
		flabf=LabelFrame(root,text=title_str)
		flabf.pack(side=LEFT,fill=Y)
		self.filter_str = filter_str
		self.skip=IntVar()
		self.f_skip_check=Checkbutton(flabf,variable=self.skip,command=self.skip_callback,text="忽略")
		self.f_skip_check.pack(side=TOP,fill=X)
		
		self.empty=IntVar()
		self.f_empty_check=Checkbutton(flabf,variable=self.empty,text="空白")
		self.f_empty_check.pack(side=TOP,fill=X)
		
		self.flist=Listbox(flabf)
		self.flist.pack(side=TOP,fill=Y)		

		self.del_f_but=Button(flabf,text="刪除",command=self.del_f_callback)
		self.del_f_but.pack(side=TOP)
		Label(flabf,text="新增"+title_str).pack(side=TOP)
		self.f_entry=Entry(flabf)
		self.f_entry.pack(side=TOP)
		self.add_f_but=Button(flabf,text="加入",command=self.add_f_callback)
		self.add_f_but.pack(side=TOP)

		self.fset=[]
		load_Filter(filter_str,self.fset)

		if(self.fset[0].find("SKIP") != -1):
			self.skip.set(1)
			self.del_f_but.configure(state=DISABLED)
			self.f_entry.configure(state=DISABLED)
			self.add_f_but.configure(state=DISABLED)
		else:
			self.skip.set(0)
			for f in self.fset[2:]:
				self.flist.insert(END,f)
		if(self.fset[1].find("EMPTY") != -1):
			self.empty.set(1)
		else:
			self.empty.set(0)
		
	def del_f_callback(self):
		self.flist.delete(self.flist.curselection()[0])
	def add_f_callback(self):
		s=self.f_entry.get()
		self.f_entry.delete(0,END)
		self.flist.insert(END,s)
	def save_filter(self):
		f=list(self.flist.get(0,END))
		if(self.skip.get()):
			f.insert(0,"SKIP")
		else:
			f.insert(0,"PICK")
		if(self.empty.get()):
			f.insert(1,"EMPTY")
		else:
			f.insert(1,"EXIST")	
		save_Filter(self.filter_str,f)

	def skip_callback(self):
		if(self.skip.get()):
			self.flist.delete(0,END)
			self.del_f_but.configure(state=DISABLED)
			self.f_entry.configure(state=DISABLED)
			self.add_f_but.configure(state=DISABLED)
		else:
			for f in self.fset[2:]:
				self.flist.insert(END,f)
			self.del_f_but.configure(state=NORMAL)
			self.f_entry.configure(state=NORMAL)
			self.add_f_but.configure(state=NORMAL)

def save_all_filter(filter_set):
	for f in filter_set:
		f.save_filter()

def save_all_filter_exit(filter_set,ftop):
	for f in filter_set:
		f.save_filter()
	ftop.destroy()

def exit_filter(ftop):
	ftop.destroy()

def set_filter_callback():
	ftop=Toplevel()
	#ftop.resizable(width=FALSE, height=FALSE)
	#ftop.geometry('{}x{}'.format(800, 600))
	ftop.title("篩選設定")

	filter_set=[]
	filter_set.append(filter_personal(ftop,"個人設定","Filter.txt"))
	filter_set.append(filter_class(ftop,"研究所設定","MS.txt"))
	filter_set.append(filter_class(ftop,"大學設定","BS.txt"))
	filter_set.append(filter_class(ftop,"科系設定","DEP.txt"))
	filter_set.append(filter_class(ftop,"希望職稱類設定","JOB.txt"))

	Button(ftop,text="離開",command=lambda:exit_filter(ftop)).pack(side=BOTTOM,fill=Y)
	Button(ftop,text="存檔離開",command=lambda:save_all_filter_exit(filter_set,ftop)).pack(side=BOTTOM,fill=Y)
	Button(ftop,text="存檔",command=lambda:save_all_filter(filter_set)).pack(side=BOTTOM,fill=Y)
	
atexit.register(exit_handler)

top=Tk()
#top.resizable(width=FALSE, height=FALSE)
#top.geometry('{}x{}'.format(1280, 768))
top.title("隨便找人")

control_set=[]

func_frame=Frame(top,bd=5)
func_frame.pack(side=TOP,fill=X)
search_lab=Label(func_frame,text="郵件目錄")
search_lab.pack(side=LEFT)
search_entry=Entry(func_frame,bd=5)
search_entry.pack(side=LEFT)
start_but=Button(func_frame,text="開始",command=lambda:start_filter_callback(control_set),bd=5,width=10)
start_but.pack(side=LEFT)
control_set.append(start_but)
filter_but=Button(func_frame,text="篩選條件設定",command=set_filter_callback,bd=5,width=10)
filter_but.pack(side=LEFT)
control_set.append(filter_but)
ver_lab=Label(func_frame,text="版本:1.03")
ver_lab.pack(side=RIGHT)


lab_frame=Frame(top)
lab_frame.pack(side=TOP,fill=BOTH)
all_check=IntVar()
all_check.set(0)
check_but=Button(lab_frame,text="選擇全部",command=lambda:all_check_callback(check_but))
check_but.grid(row=0,column=0)
control_set.append(check_but)
open_lab=Label(lab_frame,width=3)
open_lab.grid(row=0,column=1)
name_lab=Label(lab_frame,text="姓名",width=10)
name_lab.grid(row=0,column=2)
years_lab=Label(lab_frame,text="年齡",width=5)
years_lab.grid(row=0,column=3)
mf_lab=Label(lab_frame,text="性別",width=5)
mf_lab.grid(row=0,column=4)
ms_lab=Label(lab_frame,text="研究所",width=20)
ms_lab.grid(row=0,column=5)
msdep_lab=Label(lab_frame,text="科系",width=20)
msdep_lab.grid(row=0,column=6)
bs_lab=Label(lab_frame,text="大學",width=20)
bs_lab.grid(row=0,column=7)
bsdep_lab=Label(lab_frame,text="科系",width=20)
bsdep_lab.grid(row=0,column=8)
djobs_lab=Label(lab_frame,text="希望職稱類",width=10)
djobs_lab.grid(row=0,column=9)
attached_lab=Label(lab_frame,text="自傳附件",width=60)
attached_lab.grid(row=0,column=10)


lab_frame.update()
list_lframe=ScrolledFrame(top)
list_lframe.pack(side=TOP,fill=BOTH,expand=1)

work_frame=Frame(top)
work_frame.pack(side=TOP,fill=X)

save_but=Button(work_frame,text="存檔",command=save_select_callback,bd=5,width=10)
save_but.pack(side=RIGHT)
control_set.append(save_but)
mail_but=Button(work_frame,text="著附信件",command=mail_select_callback,bd=5,width=10)
mail_but.pack(side=RIGHT)
control_set.append(mail_but)

top.mainloop()




    


